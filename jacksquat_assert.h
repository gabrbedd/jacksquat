/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef JACKSQUAT_ASSERT_H
#define JACKSQUAT_ASSERT_H

#include "jacksquat.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Unconditionally terminate the program.
 *
 * This assertion will unconditionally end the program, always... if
 * the condition fails.  This should be used sparingly, using instead
 * one of the asserts that are run-time configured.
 *
 * @param truth - if zero, then the assertion will be triggered.
 * Otherwise the program will proceed as normal.
 */
void assert_always(bool truth) JACKSQUAT_PRIVATE;

/**
 * Terminate the program because a deprecated interface has been used.
 *
 * This is a configurable assertion.  When called and active, it will
 * terminate the program.  It should only appear inside deprecated
 * interfaces.
 */
void assert_deprecated() JACKSQUAT_PRIVATE;

/**
 * Terminate the program if not inside the process callback.
 *
 * This is a configurable assertion.  When called and active, it will
 * terminate the program if a process-only function is used outside of
 * the process callback.
 */
void assert_process_callback() JACKSQUAT_PRIVATE;

/**
 * Terminate the program if inside the process callback.
 *
 * This is a configurable assertion.  When called and active, it will
 * terminate the program if a forbidden function is called from
 * within the process callback.  Note that this only applies to
 * JACK API functions.  (It does not detect malloc(), printf(), etc.)
 */
void assert_not_process_callback() JACKSQUAT_PRIVATE;

/**
 * Give an unconditional warning to the user, with the given error message.
 */
void warn_always(const char* msg) JACKSQUAT_PRIVATE;

#ifdef __cplusplus
}
#endif

#endif // JACKSQUAT_ASSERT_H
