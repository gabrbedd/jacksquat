/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <jack/transport.h>
#include "jacksquat_assert.h"

extern "C"
int  jack_release_timebase (jack_client_t *client)
{
    return 0;
}

extern "C"
int  jack_set_sync_callback (jack_client_t *client,
			     JackSyncCallback sync_callback,
			     void *arg)
{
    return 0;
}

extern "C"
int  jack_set_sync_timeout (jack_client_t *client,
			    jack_time_t timeout)
{
    return 0;
}

extern "C"
int  jack_set_timebase_callback (jack_client_t *client,
				 int conditional,
				 JackTimebaseCallback timebase_callback,
				 void *arg)
{
    return 0;
}

extern "C"
int  jack_transport_locate (jack_client_t *client,
			    jack_nframes_t frame)
{
    return 0;
}

extern "C"
jack_transport_state_t jack_transport_query (const jack_client_t *client,
					     jack_position_t *pos)
{
    return (jack_transport_state_t)0;
}

extern "C"
jack_nframes_t jack_get_current_transport_frame (const jack_client_t *client)
{
    return (jack_nframes_t)0;
}

extern "C"
int  jack_transport_reposition (jack_client_t *client,
				jack_position_t *pos)
{
}

extern "C"
void jack_transport_start (jack_client_t *client)
{
}

extern "C"
void jack_transport_stop (jack_client_t *client)
{
}

	
/* DEPRECATED */
extern "C"
void jack_get_transport_info (jack_client_t *client,
			      jack_transport_info_t *tinfo)
{
    assert_deprecated();
}

/* DEPRECATED */
extern "C"
void jack_set_transport_info (jack_client_t *client,
			      jack_transport_info_t *tinfo)
{
    assert_deprecated();
}
