/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <jack/statistics.h>

extern "C"
float jack_get_max_delayed_usecs (jack_client_t *client)
{
    return 0.0f;
}

extern "C"
float jack_get_xrun_delayed_usecs (jack_client_t *client)
{
    return 0.0f;
}

extern "C"
void jack_reset_max_delayed_usecs (jack_client_t *client)
{
}
