/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Thread.hpp"

#include <pthread.h>
#include <iostream>
#include <cstring>
#include <errno.h>
#include <cassert>

using namespace std;

namespace JackSquat
{
    Thread::Thread() :
        m_realtime(false),
        m_main(0),
        m_main_arg(0)
    {
        memset((void*)&m_thread, 0, sizeof(pthread_t));
    }

    Thread::~Thread()
    {
        realtime(false);
        cancel();
        join();
    }

    void Thread::start()
    {
        int stat;
        if(m_main) {
            stat = pthread_create(&m_thread, 0, Thread::static_run, this);
            if(stat != 0) {
                switch(stat) {
                case EAGAIN:
                    cerr << "pthread_create() returned `EAGAIN`" << endl;
                    break;
                case EINVAL:
                    cerr << "pthread_create() flagged the `attr` param"
                        " as invalid." << endl;
                    break;
                case EPERM:
                    cerr << "Insufficient permission to set scheduling"
                        " on thread with pthread_create()" << endl;
                    break;
                default:
                    cerr << "pthread_create() returned an unknown error."
                         << endl;
                }
                assert(stat == 0);
            }
        }
    }

    void Thread::cancel()
    {
        /* ignores return */
        pthread_cancel(m_thread);
    }

    void Thread::join()
    {
        /* ignores return */
        pthread_join(m_thread, 0);
    }

    void Thread::set_run_main(ThreadMain m, void* arg)
    {
        m_main = m;
        m_main_arg = arg;
    }

    void Thread::realtime(bool rt)
    {
        m_realtime = rt;
    }

    bool Thread::realtime() const
    {
        return m_realtime;
    }

    void Thread::is_the_process_thread(bool pt)
    {
        m_is_process_thread = pt;
    }

    bool Thread::is_the_process_thread() const
    {
        return m_is_process_thread;
    }

    void Thread::run()
    {
        pthread_key_t key;
        int stat;
        assert(m_main);
        key = current_thread_key();
        stat = pthread_setspecific(key, this);
        assert(stat == 0);
        m_main(m_main_arg);
        pthread_setspecific(key, 0);
    }

    void* Thread::static_run(void* arg)
    {
        Thread *that = reinterpret_cast<Thread*>(arg);
        that->run();
        return 0;
    }

    Thread* Thread::current_thread()
    {
        pthread_key_t key;
        void *data;
        key = current_thread_key();
        data = pthread_getspecific(key);
        if(data)
            return reinterpret_cast<Thread*>(data);
        return 0;
    }

    static pthread_key_t tls_key;

    static void thread_key_alloc()
    {
        pthread_key_t key;
        int stat;
        stat = pthread_key_create(&key, 0);
        assert(stat == 0);
        tls_key = key;
    }


    pthread_key_t Thread::current_thread_key()
    {
        static bool initialized = false;
        static pthread_key_t key;
        if(!initialized) {
            initialized = true;
            pthread_once_t once_control = PTHREAD_ONCE_INIT;
            pthread_once(&once_control, thread_key_alloc);
            key = tls_key;
        }
        return key;
    }

} // namespace JackSquat
