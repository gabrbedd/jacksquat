/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef JACKSQUAT_THREAD_HPP
#define JACKSQUAT_THREAD_HPP

#include "jacksquat.h"
#include <pthread.h>

namespace JackSquat
{
    typedef void (*ThreadMain)(void* arg);

    class JACKSQUAT_PRIVATE Thread
    {
    public:
        Thread();
        virtual ~Thread();

        void start();
        void cancel();
        void join();
        void set_run_main(ThreadMain m, void* arg);


        void realtime(bool rt);
        bool realtime() const;

        void is_the_process_thread(bool pt);
        bool is_the_process_thread() const;

        static Thread* current_thread();

    private:
        void run();
        static void* static_run(void*);
        static pthread_key_t current_thread_key();

    private:
        pthread_t m_thread;
        bool m_realtime;
        bool m_is_process_thread;
        ThreadMain m_main;
        void* m_main_arg;

    }; // class Thread

} // namespace JackSquat

#endif // JACKSQUAT_THREAD_HPP
