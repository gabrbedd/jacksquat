/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <jack/session.h>

extern "C"
int jack_set_session_callback (jack_client_t       *client,
                               JackSessionCallback  session_callback,
                               void                *arg)
{
    return 0;
}

extern "C"
int jack_session_reply (jack_client_t        *client,
                        jack_session_event_t *event)
{
    return 0;
}

extern "C"
void jack_session_event_free (jack_session_event_t *event)
{
}


extern "C"
char *jack_client_get_uuid (jack_client_t *client)
{
    return 0;
}


extern "C"
jack_session_command_t *jack_session_notify (
    jack_client_t*             client,
    const char                *target,
    jack_session_event_type_t  type,
    const char                *path)
{
    return 0;
}

extern "C"
void jack_session_commands_free (jack_session_command_t *cmds)
{
}

extern "C"
char *jack_get_uuid_for_client_name (jack_client_t *client,
                                     const char    *client_name)
{
    return 0;
}

extern "C"
char *jack_get_client_name_by_uuid (jack_client_t *client,
                                    const char    *client_uuid )
{
    return 0;
}

extern "C"
int jack_reserve_client_name (jack_client_t *client,
                              const char    *name,
                              const char    *uuid)
{
    return 0;
}

extern "C"
int jack_client_has_session_callback (jack_client_t *client, const char *client_name)
{
    return 0;
}
