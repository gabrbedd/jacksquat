/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <jack/midiport.h>

extern "C"
jack_nframes_t jack_midi_get_event_count(void* port_buffer)
{
    return 0;
}

extern "C"
int jack_midi_event_get(jack_midi_event_t *event,
                        void              *port_buffer,
                        jack_nframes_t     event_index)
{
    return 0;
}

extern "C"
void jack_midi_clear_buffer(void *port_buffer)
{
}

extern "C"
size_t jack_midi_max_event_size(void* port_buffer)
{
}


extern "C"
jack_midi_data_t* jack_midi_event_reserve(void           *port_buffer,
                                          jack_nframes_t  time, 
                                          size_t          data_size)
{
    return 0;
}

extern "C"
int jack_midi_event_write(void                   *port_buffer,
                          jack_nframes_t          time,
                          const jack_midi_data_t *data,
                          size_t                  data_size)
{
    return 0;
}

extern "C"
jack_nframes_t jack_midi_get_lost_event_count(void *port_buffer)
{
    return 0;
}
