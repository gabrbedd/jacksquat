/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef JACKSQUAT_MUTEX_HPP
#define JACKSQUAT_MUTEX_HPP

#include <pthread.h>
#include <errno.h>
#include <cassert>
#include <stdexcept>

namespace JackSquat
{
    class mutex_error : public std::logic_error
    {
    public:
        explicit mutex_error(const std::string& what) : std::logic_error(what) {}
    };

    class Mutex
    {
        pthread_mutex_t m_mutex;
        pthread_mutexattr_t m_attr;

    public:
        Mutex() throw(mutex_error) {
            pthread_mutexattr_init(&m_attr);
            if(pthread_mutexattr_settype(&m_attr, PTHREAD_MUTEX_ERRORCHECK_NP)) {
                throw mutex_error("Could not set mutex attribute to PTHREAD_MUTEX_ERRORCHECK_NP");
            }
            pthread_mutex_init(&m_mutex, &m_attr); // always returns 0
        }

        ~Mutex() throw() {
            pthread_mutex_destroy(&m_mutex);
            pthread_mutexattr_destroy(&m_attr);
        }

        /* Returns true on success */
        bool lock() throw(mutex_error) {
            int rv;
            rv = pthread_mutex_lock(&m_mutex);
            if(rv) {
                switch (rv) {
                case EINVAL:
                    throw mutex_error("Trying to lock an invalid mutex.  This is probably a "
                                      "bug in JackSquat::Mutex. Contact the developers.");
                    break;
                case EDEADLK:
                    throw mutex_error("A deadlock has been detected.");
                    break;
                default:
                    throw mutex_error("An unknown error has happened with Mutex::lock()");
                    break;
                }
            }
            return (rv == 0);
        }

        /* Returns true on success */
        bool try_lock() throw(mutex_error) {
            int stat;
            stat = pthread_mutex_trylock(&m_mutex);
            if(stat) {
                switch(stat) {
                case EINVAL:
                    throw mutex_error("Trying to lock an invalid mutex.  This is probably a "
                                      "bug in JackSquat::Mutex. Contact the developers.");
                    break;
                case EBUSY:
                    /* not an error */
                    break;
                default:
                    throw mutex_error("An unknown error has happened with Mutex::lock()");
                    break;
                }
            }
            return (0 == stat);
        }

        /* Returns true on success */
        bool unlock() throw(mutex_error) {
            int stat;
            stat = pthread_mutex_unlock(&m_mutex);
            if(stat) {
                switch(stat) {
                case EINVAL:
                    throw mutex_error("Trying to lock an invalid mutex.  This is probably a "
                                      "bug in JackSquat::Mutex. Contact the developers.");
                    break;
                case EPERM:
                    throw mutex_error("Tried to unlock mutex from a different thread.");
                    break;
                default:
                    throw mutex_error("An unknown error has happened with Mutex::lock()");
                    break;
                }
            }

            return (0 == stat);
        }

    };

} // namespace JackSquat

#endif // JACKSQUAT_MUTEX_HPP
