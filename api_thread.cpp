/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <jack/types.h>
#include <jack/thread.h>

extern "C"
int jack_client_real_time_priority (jack_client_t *client)
{
    return -1;
}

extern "C"
int jack_client_max_real_time_priority (jack_client_t *client)
{
    return -1;
}

extern "C"
int jack_acquire_real_time_scheduling (jack_native_thread_t thread, int priority)
{
    return -1;
}

extern "C"
int jack_client_create_thread (jack_client_t* client,
			       jack_native_thread_t *thread,
			       int priority,
			       int realtime,	/* boolean */
			       void *(*start_routine)(void*),
			       void *arg)
{
    return -1;
}

extern "C"
int jack_drop_real_time_scheduling (jack_native_thread_t thread)
{
    return -1;
}

extern "C"
void jack_set_thread_creator (jack_thread_creator_t creator)
{
}
