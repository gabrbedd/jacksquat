/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef JACKSQUAT_CONFIG_HPP
#define JACKSQUAT_CONFIG_HPP

#include "jacksquat.h"

namespace JackSquat
{
    class Config;

    template <typename T>
    class ConfigProperty
    {
    public:
        friend class Config;

        ConfigProperty(T d = T()) : m_d(d) {}
        ~ConfigProperty() {}

        const T& operator() () const { return m_d; }

    private:
        void operator() (const T& d) {
            m_d = d;
        }

    private:
        T m_d;
    };

    class JACKSQUAT_PRIVATE Config
    {
    public:
        /* The default constructor will call init() */
        Config();
        virtual ~Config();

        /**
         * Looks at environment variable JACKSQUAT_CONFIG_ARGS for
         * command-line-like configuration.
         */
        void init();

        /**
         * Whether or not the use of deprecated interfaces will cause
         * an assert failure.
         *
         * -enforce-deprecated (true, default)
         * -no-enforce-deprecated (false)
         */
        ConfigProperty<bool> enforce_deprecated;

        /**
         * Whether or not an assert will fail if a non-process-callback
         * method is used in the process callback, or vice versa.
         *
         * -enforce-callback-api-rules (true, default)
         * -no-enforce-callback-api-rules (false)
         */
        ConfigProperty<bool> enforce_callback_api_rules;

    }; // class Thread

    /* The global config instance. */
    Config* config() JACKSQUAT_PRIVATE;

} // namespace JackSquat

#endif // JACKSQUAT_CONFIG_HPP
