/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Config.hpp"

#include <cstdlib>
#include <memory>
#include <string>

namespace JackSquat
{
    Config* config()
    {
        static std::auto_ptr<Config> instance;
        if( ! instance.get() ) {
            instance.reset( new Config );
        }
        return instance.get();
    }

    Config::Config() :
        enforce_deprecated(true),
        enforce_callback_api_rules(true)
    {
        init();
    }

    Config::~Config()
    {
    }

    void Config::init()
    {
        static const char delim[] = " \t";
        char *args_;
        args_ = getenv("JACKSQUAT_CONFIG_ARGS");

        if(!args_)
            return;

        std::string args(args_);
        size_t start, end;
        start = 0;

        end = args.find_first_of(delim, start);
        std::string sub;
        size_t sub_len;
        while( start < args.length() ) {
            sub = "";
            if( start == end ) {
                goto NEXT_CYCLE;
            }

            if(end == std::string::npos) {
                sub_len = std::string::npos;
            } else {
                sub_len = end - start;
            }
            sub = args.substr(start, sub_len);
            if( sub.length() == 0 ) {
                goto NEXT_CYCLE;
            }
            if( std::string::npos == sub.find_first_not_of(delim, 0) ) {
                goto NEXT_CYCLE;
            }

            if( sub == "-enforce-deprecated" ) {
                enforce_deprecated(true);
            } else if ( sub == "-no-enforce-deprecated" ) {
                enforce_deprecated(false);
            } else if ( sub == "-enforce-callback-api-rules" ) {
                enforce_callback_api_rules(true);
            } else if ( sub == "-no-enforce-callback-api-rules" ) {
                enforce_callback_api_rules(false);
            }

        NEXT_CYCLE:
            if( end != std::string::npos )
                start = end + 1;
            else
                break;
            if(start >= args.length())
                break;
            end = args.find_first_of(delim, start);
        }
    }

} // namespace JackSquat
