/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <jack/jack.h>
#include "jacksquat_assert.h"

#include "Server.hpp"
#include "Client.hpp"

using namespace JackSquat;

extern "C"
jack_client_t *jack_client_open (const char *client_name,
				 jack_options_t options,
				 jack_status_t *status, ...)
{
    /* Currently, the varags are only for selecting a named
     * jack server instance.  This is not supported for JackSquat.
     */
    assert_not_process_callback();
    return server()->client_open(client_name, options, status)->jack_cast();
}

/* DEPRECATED */
extern "C"
jack_client_t *jack_client_new (const char *client_name)
{
    assert_deprecated();
    return 0;
}

extern "C"
int jack_client_close (jack_client_t *client)
{
    assert_not_process_callback();
    Client *c = reinterpret_cast<Client*>(client);
    Server *s = c->server();
    return s->client_close( c );
}

extern "C"
int jack_client_name_size (void)
{
    return JACKSQUAT_MAX_CLIENT_NAME;
}

extern "C"
char *jack_get_client_name (jack_client_t *client)
{
    Client *c = reinterpret_cast<Client*>(client);
    return const_cast<char*>(c->name());
}

/* DEPRECATED */
extern "C"
int jack_internal_client_new (const char *client_name,
			      const char *load_name,
			      const char *load_init)
{
    assert_deprecated();
    return 0;
}

/* DEPRECATED */
extern "C"
void jack_internal_client_close (const char *client_name)
{
    assert_deprecated();
}

extern "C"
int jack_activate (jack_client_t *client)
{
    assert_not_process_callback();
    Client *c = reinterpret_cast<Client*>(client);
    Server *s = c->server();
    return s->activate(c);
}

extern "C"
int jack_deactivate (jack_client_t *client)
{
    assert_not_process_callback();
    Client *c = reinterpret_cast<Client*>(client);
    Server *s = c->server();
    return s->deactivate(c);
}

extern "C"
jack_native_thread_t jack_client_thread_id (jack_client_t * client)
{
    return 0;
}

extern "C"
int jack_is_realtime (jack_client_t *client)
{
    return 0;
}

/* DEPRECATED */
extern "C"
jack_nframes_t jack_thread_wait (jack_client_t* client, int status)
{
    assert_deprecated();
    return 0;
}

extern "C"
jack_nframes_t jack_cycle_wait (jack_client_t* client)
{
    return 0;
}

extern "C"
void jack_cycle_signal (jack_client_t* client, int status)
{
}
	
extern "C"
int jack_set_process_thread(jack_client_t* client, JackThreadCallback fun, void *arg)
{
    return 0;
}

extern "C"
int jack_set_thread_init_callback (jack_client_t *client,
				   JackThreadInitCallback thread_init_callback,
				   void *arg)
{
    return 0;
}

extern "C"
void jack_on_shutdown (jack_client_t *client,
		       JackShutdownCallback function, void *arg)
{
}

extern "C"
void jack_on_info_shutdown (jack_client_t *client,
			    JackInfoShutdownCallback function, void *arg)
{
}

extern "C"
int jack_set_process_callback (jack_client_t *client,
			       JackProcessCallback process_callback,
			       void *arg)
{
    assert_not_process_callback();
    Client *c = reinterpret_cast<Client*>(client);
    return c->set_process_callback(process_callback, (void*)arg);
}

extern "C"
int jack_set_freewheel_callback (jack_client_t *client,
				 JackFreewheelCallback freewheel_callback,
				 void *arg)
{
    return 0;
}

extern "C"
int jack_set_buffer_size_callback (jack_client_t *client,
				   JackBufferSizeCallback bufsize_callback,
				   void *arg)
{
    return 0;
}

extern "C"
int jack_set_sample_rate_callback (jack_client_t *client,
				   JackSampleRateCallback srate_callback,
				   void *arg)
{
    return 0;
}

extern "C"
int jack_set_client_registration_callback (jack_client_t *client,
					   JackClientRegistrationCallback
					   registration_callback, void *arg)
{
    return 0;
}
	
extern "C"
int jack_set_port_registration_callback (jack_client_t *client,
					 JackPortRegistrationCallback
					 registration_callback, void *arg)
{
    return 0;
}

extern "C"
int jack_set_port_connect_callback (jack_client_t *client,
				    JackPortConnectCallback
				    connect_callback, void *arg)
{
    return 0;
}

extern "C"
int jack_set_graph_order_callback (jack_client_t *client,
				   JackGraphOrderCallback graph_callback,
				   void * arg)
{
    return 0;
}

extern "C"
int jack_set_xrun_callback (jack_client_t *client,
			    JackXRunCallback xrun_callback, void *arg)
{
    return 0;
}

extern "C"
int jack_set_latency_callback (jack_client_t *client,
			       JackLatencyCallback latency_callback,
			       void *arg)
{
    return 0;
}

extern "C"
int jack_set_freewheel(jack_client_t* client, int onoff)
{
    return 0;
}

extern "C"
int jack_set_buffer_size (jack_client_t *client, jack_nframes_t nframes)
{
    return 0;
}

extern "C"
jack_nframes_t jack_get_sample_rate (jack_client_t *client)
{
    return 0;
}

extern "C"
jack_nframes_t jack_get_buffer_size (jack_client_t *client)
{
    return 0;
}

extern "C"
int  jack_engine_takeover_timebase (jack_client_t *client)
{
    assert_deprecated();
    return 0;
}

extern "C"
float jack_cpu_load (jack_client_t *client)
{
    return 0.0f;
}
	
extern "C"
jack_port_t *jack_port_register (jack_client_t *client,
                                 const char *port_name,
                                 const char *port_type,
                                 unsigned long flags,
                                 unsigned long buffer_size)
{
    return 0;
}

extern "C"
int jack_port_unregister (jack_client_t *client, jack_port_t *port)
{
    return 0;
}

extern "C"
void *jack_port_get_buffer (jack_port_t *port, jack_nframes_t nframes)
{
    return 0;
}

extern "C"
const char *jack_port_name (const jack_port_t *port)
{
    return 0;
}

extern "C"
const char *jack_port_short_name (const jack_port_t *port)
{
    return 0;
}

extern "C"
int jack_port_flags (const jack_port_t *port)
{
    return 0;
}

extern "C"
const char *jack_port_type (const jack_port_t *port)
{
    return 0;
}

extern "C"
int jack_port_is_mine (const jack_client_t *client, const jack_port_t *port)
{
    return 0;
}

extern "C"
int jack_port_connected (const jack_port_t *port)
{
    return 0;
}

extern "C"
int jack_port_connected_to (const jack_port_t *port,
			    const char *port_name)
{
    return 0;
}

extern "C"
const char **jack_port_get_connections (const jack_port_t *port)
{
    return 0;
}

extern "C"
const char **jack_port_get_all_connections (const jack_client_t *client,
					    const jack_port_t *port)
{
    return 0;
}

/* DEPRECATED */
extern "C"
int  jack_port_tie (jack_port_t *src, jack_port_t *dst)
{
    return 0;
}

/* DEPRECATED */
extern "C"
int  jack_port_untie (jack_port_t *port)
{
    return 0;
}

extern "C"
int jack_port_set_name (jack_port_t *port, const char *port_name)
{
    return 0;
}

extern "C"
int jack_port_set_alias (jack_port_t *port, const char *alias)
{
    return 0;
}

extern "C"
int jack_port_unset_alias (jack_port_t *port, const char *alias)
{
    return 0;
}

extern "C"
int jack_port_get_aliases (const jack_port_t *port, char* const aliases[2])
{
    return 0;
}

extern "C"
int jack_port_request_monitor (jack_port_t *port, int onoff)
{
    return 0;
}

extern "C"
int jack_port_request_monitor_by_name (jack_client_t *client,
				       const char *port_name, int onoff)
{
    return 0;
}

extern "C"
int jack_port_ensure_monitor (jack_port_t *port, int onoff)
{
    return 0;
}

extern "C"
int jack_port_monitoring_input (jack_port_t *port)
{
    return 0;
}

extern "C"
int jack_connect (jack_client_t *client,
		  const char *source_port,
		  const char *destination_port)
{
    return 0;
}


extern "C"
int jack_disconnect (jack_client_t *client,
		     const char *source_port,
		     const char *destination_port)
{
    return 0;
}

extern "C"
int jack_port_disconnect (jack_client_t *client, jack_port_t *port)
{
    return 0;
}

extern "C"
int jack_port_name_size(void)
{
    return 0;
}

extern "C"
int jack_port_type_size(void)
{
    return 0;
}

extern "C"
size_t jack_port_type_get_buffer_size (jack_client_t *client, const char *port_type)
{
    return 0;
}

/* DEPRECATED */
extern "C"
void jack_port_set_latency (jack_port_t *port, jack_nframes_t nframes)
{
    assert_deprecated();
}
	
extern "C"
void jack_port_get_latency_range (jack_port_t *port, jack_latency_callback_mode_t mode, jack_latency_range_t *range)
{
}

extern "C"
void jack_port_set_latency_range (jack_port_t *port, jack_latency_callback_mode_t mode, jack_latency_range_t *range)
{
}

extern "C"
int jack_recompute_total_latencies (jack_client_t *client)
{
    return 0;
}

/* DEPRECATED */
extern "C"
jack_nframes_t jack_port_get_latency (jack_port_t *port)
{
    assert_deprecated();
    return 0;
}

/* DEPRECATED */
extern "C"
jack_nframes_t jack_port_get_total_latency (jack_client_t *client,
					    jack_port_t *port)
{
    assert_deprecated();
    return 0;
}

/* DEPRECATED */
extern "C"
int jack_recompute_total_latency (jack_client_t *client, jack_port_t *port)
{
    assert_deprecated();
    return 0;
}

extern "C"
const char **jack_get_ports (jack_client_t *client, 
			     const char *port_name_pattern, 
			     const char *type_name_pattern, 
			     unsigned long flags)
{
    return 0;
}

extern "C"
jack_port_t *jack_port_by_name (jack_client_t *client, const char *port_name)
{
    return 0;
}

extern "C"
jack_port_t *jack_port_by_id (jack_client_t *client,
			      jack_port_id_t port_id)
{
    return 0;
}

extern "C"
jack_nframes_t jack_frames_since_cycle_start (const jack_client_t *client)
{
    return 0;
}

extern "C"
jack_nframes_t jack_frame_time (const jack_client_t *client)
{
    return 0;
}

extern "C"
jack_nframes_t jack_last_frame_time (const jack_client_t *client)
{
    return 0;
}

extern "C"
jack_time_t jack_frames_to_time(const jack_client_t *client, jack_nframes_t nframes)
{
    return 0;
}

extern "C"
jack_nframes_t jack_time_to_frames(const jack_client_t *client, jack_time_t time)
{
    return 0;
}

extern "C"
jack_time_t jack_get_time()
{
    return 0;
}

extern "C"
void jack_set_error_function (void (*func)(const char *))
{
}

extern "C"
void jack_set_info_function (void (*func)(const char *))
{
}

extern "C"
void jack_free(void* ptr)
{
}
