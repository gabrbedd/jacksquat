/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <jack/intclient.h>

extern "C"
char *jack_get_internal_client_name (jack_client_t *client,
				     jack_intclient_t intclient)
{
    return 0;
}

extern "C"
jack_intclient_t jack_internal_client_handle (jack_client_t *client,
					      const char *client_name,
					      jack_status_t *status)
{
    return 0;
}

extern "C"
jack_intclient_t jack_internal_client_load (jack_client_t *client,
					    const char *client_name,
					    jack_options_t options,
					    jack_status_t *status, ...)
{
    return 0;
}

extern "C"
jack_status_t jack_internal_client_unload (jack_client_t *client,
					   jack_intclient_t intclient)
{
    return (jack_status_t)0;
}
