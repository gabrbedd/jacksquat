#include "Config.hpp"

#ifdef NDEBUG
#undef NDEBUG
#endif

#include <cassert>
#include <iostream>
#include <cstdlib>

using namespace std;
using namespace JackSquat;

const char CONFIG_ARGS[] = "JACKSQUAT_CONFIG_ARGS";

void t_000_sanity()
{
    unsetenv(CONFIG_ARGS);
    assert( 0 == getenv(CONFIG_ARGS) );

    Config c;
    assert( c.enforce_deprecated() );
    assert( c.enforce_callback_api_rules() );
}

/* "simple" as in "hand-picked cases".  This is opposed to a combinatorial approach. */
void t_001_simple_tests()
{
    int stat;

    unsetenv(CONFIG_ARGS);
    assert( 0 == getenv(CONFIG_ARGS) );

    {
        stat = setenv(CONFIG_ARGS, "-foo", 1);
        Config c;
        assert( c.enforce_deprecated() );
        assert( c.enforce_callback_api_rules() );
        unsetenv(CONFIG_ARGS);
        assert( 0 == getenv(CONFIG_ARGS) );
    }
    {
        stat = setenv(CONFIG_ARGS, "-enforce-deprecated", 1);
        Config c;
        assert( c.enforce_deprecated() );
        assert( c.enforce_callback_api_rules() );
        unsetenv(CONFIG_ARGS);
        assert( 0 == getenv(CONFIG_ARGS) );
    }
    {
        stat = setenv(CONFIG_ARGS, "-enforce-deprecated -no-enforce-deprecated", 1);
        Config c;
        assert( ! c.enforce_deprecated() );
        assert( c.enforce_callback_api_rules() );
        unsetenv(CONFIG_ARGS);
        assert( 0 == getenv(CONFIG_ARGS) );
    }
    {
        stat = setenv(CONFIG_ARGS, "   -no-enforce-deprecated    \t \t  -enforce-deprecated", 1);
        Config c;
        assert( c.enforce_deprecated() );
        assert( c.enforce_callback_api_rules() );
        unsetenv(CONFIG_ARGS);
        assert( 0 == getenv(CONFIG_ARGS) );
    }
    {
        stat = setenv(CONFIG_ARGS, "   -no-enforce-callback-api-rules\t-no-enforce-deprecated", 1);
        Config c;
        assert( ! c.enforce_deprecated() );
        assert( ! c.enforce_callback_api_rules() );
        unsetenv(CONFIG_ARGS);
        assert( 0 == getenv(CONFIG_ARGS) );
    }
    {
        stat = setenv(CONFIG_ARGS, "   -no-enforce-callback-api-rules\t-enforce-callback-api-rules", 1);
        Config c;
        assert( c.enforce_deprecated() );
        assert( c.enforce_callback_api_rules() );
        unsetenv(CONFIG_ARGS);
        assert( 0 == getenv(CONFIG_ARGS) );
    }
    {
        /* Note:  the shim script (jacksquat) is supposed to filter out `fuzz-stuff` for us.
         * so... Config will happily parse everything given in the environment varuable
         * without question.
         */
        stat = setenv(CONFIG_ARGS, "   -no-enforce-callback-api-rules\t-no-enforce-deprecated\t\t \t"
                      "fuzz-stuff -enforce-callback-api-rules -enforce-deprecated", 1);
        Config c;
        assert( c.enforce_deprecated() );
        assert( c.enforce_callback_api_rules() );
        unsetenv(CONFIG_ARGS);
        assert( 0 == getenv(CONFIG_ARGS) );
    }
}

int main(int argc, char* argv[])
{
    t_000_sanity();
    t_001_simple_tests();
    return 0;
}
