/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Mutex.hpp"
#include "MutexLocker.hpp"
#include "Thread.hpp"

#ifdef NDEBUG
#undef NDEBUG
#endif

#include <cassert>
#include <iostream>
#include <unistd.h>
#include <sys/syscall.h>

using namespace std;
using namespace JackSquat;

void t_000_sanity()
{
    {
        Mutex m;
        assert( m.lock() );
        assert( m.unlock() );
        assert( m.try_lock() );
        assert( ! m.try_lock() );
        assert( m.unlock() );
    }
    {
        Mutex m;
        {
            MutexLocker lock(m);
            assert( ! m.try_lock() );
            assert( lock.unlock() );
            assert( lock.relock() );
            assert( lock.unlock() );
            assert( lock.try_lock() );
            assert( ! lock.try_lock() );
        }
        assert( m.lock() );
        assert( m.unlock() );
    }
}

void t_010_mutex_exceptions()
{
    {
        /* Locking a locked mutex */
        Mutex m;
        m.lock();
        try {
            m.lock();
            assert(false); /* FAIL */
        } catch(mutex_error& e) {
            /* PASS */
        }
    }
    {
        /* Unlocking a unlocked mutex */
        Mutex m;
        try {
            m.unlock();
            assert(false); /* FAIL */
        } catch(mutex_error& e) {
            /* PASS */
        }
    }
    {
        /* MutexLocker should start off locked */
        Mutex m;
        MutexLocker lock(m);
        try {
            lock.relock();
            assert(false); /* FAIL */
        } catch(mutex_error& e) {
            /* PASS */
        }
    }
    {
        /* MutexLocker double unlock */
        Mutex m;
        MutexLocker lock(m);
        lock.unlock();
        try {
            lock.unlock();
            assert(false); /* FAIL */
        } catch(mutex_error& e) {
            /* PASS */
        }
    }
}

bool start_t_020 = false;
const unsigned long iter_t_020 = (1L<<23);
unsigned long ctr_t_020 = 0;

struct t_020_main_data {
    Mutex *m;
    unsigned *cpu;
};

void t_020_main(void* arg)
{
    t_020_main_data *data = reinterpret_cast<t_020_main_data*>(arg);
    Mutex *m = data->m;

    syscall(SYS_getcpu, data->cpu, 0, 0);

    unsigned long k;
    while( ! start_t_020 ); // busy_wait

    for( k=0 ; k<iter_t_020 ; ++k ) {
        m->lock();
        ctr_t_020 += 1;
        m->unlock();
    }
}

void t_020_test_with_threads()
{
    /* Note: this test will typically take about 3 seconds */
    Thread t1, t2;
    Mutex m;
    unsigned cpu[2] = {~0, ~0};
    t_020_main_data data[2] = {
        {&m, &cpu[0]},
        {&m, &cpu[1]}
    };

    ctr_t_020 = 0;
    assert( ctr_t_020 == 0 );
    t1.set_run_main(t_020_main, (void*)&data[0]);
    t2.set_run_main(t_020_main, (void*)&data[1]);
    start_t_020 = true;
    t1.start();
    t2.start();
    t2.join();
    t1.join();
    cout << "sync'd ctr_t_020=" << hex << ctr_t_020 << endl;
    assert( cpu[0] < ~0 );
    assert( cpu[1] < ~0 );
    assert( ctr_t_020 == 2*iter_t_020 );
}

void t_030_fail_with_threads()
{
    /* Note: this test will typically take about 3 seconds */

    /* If t_020 worked with locks, t_030 should fail without them.
     * However, it will only fail on multi-processor|core|thread
     * machines.  Therefore, if it can't detect that you have one,
     * it will simply run through the test and pass it.
     */

    Thread t1, t2;
    Mutex m1, m2;
    unsigned cpu[2] = {~0, ~0};
    t_020_main_data data[2] = {
        {&m1, &cpu[0]},
        {&m2, &cpu[1]}
    };

    ctr_t_020 = 0;
    assert( ctr_t_020 == 0 );
    t1.set_run_main(t_020_main, (void*)&data[0]);
    t2.set_run_main(t_020_main, (void*)&data[1]);
    start_t_020 = true;
    t1.start();
    t2.start();
    t2.join();
    t1.join();
    cout << "unsync'd ctr_t_020=" << hex << ctr_t_020 << endl;
    assert( cpu[0] < ~0 );
    assert( cpu[1] < ~0 );
    if( cpu[0] != cpu[1] ) {
        assert( ctr_t_020 != 2*iter_t_020 );
    } else {
        cout << "Can't really test mutexes exhaustively on signle-processor system" << endl;
    }
}

int main(int /*argc*/, char* /*argv*/ [])
{
    try {
        t_000_sanity();
        t_010_mutex_exceptions();
        t_020_test_with_threads();
        t_030_fail_with_threads();
    } catch(mutex_error& e) {
        cout << "EXCEPTION: " << e.what() << endl;
        cout << "This was a JackSquat::mutex_error... so this should not have slipped through." << endl;
        return 1;
    } catch(std::exception& e) {
        cout << "EXCEPTION: " << e.what() << endl;
        return 1;
    } catch(...) {
        cout << "An unknown exception occured" << endl;
        return 1;
    }

    return 0;
}
