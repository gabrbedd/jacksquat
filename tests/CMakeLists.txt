
CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

ENABLE_TESTING()

## TESTING UTILITIES

CONFIGURE_FILE(setup_environment.in
  setup_environment
  @ONLY
)
SET(EXPECT_FAILURE ${CMAKE_CURRENT_SOURCE_DIR}/expect_failure)
SET(ENV ${CMAKE_CURRENT_BUILD_DIR}/setup_environment)
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR})

## TEST ORGANIZATION
##
## 0nnn = sanity tests (is there gas in the tank)
## 1nnn = basic internal types tests
## 2nnn = internal class tests
## 3nnn = jack api completeness tests
## 4nnn = error detection tests (i.e catch the stuff we're supposed to be catching
##

#############################################################
# 0000 tests = Make sure that CTest works right
#############################################################

# Always passes.
ADD_TEST(0000_pass ${ENV} ${CMAKE_CURRENT_SOURCE_DIR}/0000_pass)
# Always fails... but we expected that.
ADD_TEST(0000_fail ${EXPECT_FAILURE} ${ENV} ${CMAKE_CURRENT_SOURCE_DIR}/0000_expected_fail)
# Make sure that we can find our built library
CONFIGURE_FILE(0000_mock_jack_in_path.in
  0000_mock_jack_in_path
)
ADD_TEST(0000_mock_jack_in_path ${ENV} ./0000_mock_jack_in_path)


#############################################################
# 1000 Config
#############################################################

# Make sure that it parses the environment properly.
ADD_EXECUTABLE(1000_config_basics
  1000_config_basics.cpp
)
TARGET_LINK_LIBRARIES(1000_config_basics
  jacksquat
  ${JACKSQUAT_LINK_LIBS}
)
ADD_TEST(1000_config_basics_t 1000_config_basics)

#############################################################
# 1010 Assertions
#############################################################

ADD_EXECUTABLE(1010_assertions_helper
  1010_assertions_helper.cpp
)
TARGET_LINK_LIBRARIES(1010_assertions_helper
  jacksquat
  ${JACKSQUAT_LINK_LIBS}
)
ADD_TEST(1010_assertions_t
  ${ENV} ${CMAKE_CURRENT_SOURCE_DIR}/1010_assertions ${CMAKE_CURRENT_BINARY_DIR}/1010_assertions_helper ${EXPECT_FAILURE}
)
SET_TESTS_PROPERTIES(1010_assertions_t
  PROPERTIES TIMEOUT 5
)

#############################################################
# 1020 Thread
#############################################################

# XXX TO-DO !!!

#############################################################
# 1030 Mutex
#############################################################

ADD_EXECUTABLE(1030_mutex
  1030_mutex.cpp
)
TARGET_LINK_LIBRARIES(1030_mutex
  jacksquat
  ${JACKSQUAT_LINK_LIBS}
)
ADD_TEST(1030_mutex_t
  ${ENV} ./1030_mutex
)
SET_TESTS_PROPERTIES(1030_mutex_t
  PROPERTIES TIMEOUT 10
)
