#include "jacksquat_assert.h"
#include "Thread.hpp"

#ifdef NDEBUG
#undef NDEBUG
#endif

#include <cassert>
#include <string>

#include <iostream>
using namespace std;
using namespace JackSquat;

void trigger_assert_always()
{
    assert_always(false);
}

void trigger_assert_deprecated()
{
    assert_deprecated();
}

void trigger_assert_process_callback()
{
    assert_process_callback();
}

void tanpc_main(void* /*arg*/)
{
    assert_not_process_callback();
}

void trigger_assert_not_process_callback()
{
    Thread t;
    t.set_run_main(tanpc_main, 0);
    t.is_the_process_thread(true);
    t.start();
    sleep(1);
    t.cancel();
    t.join();
}

void trigger_warn_always()
{
    warn_always("this is a test");
}

int main(int argc, char* argv[])
{
    if(argc != 2) {
        cout << "usage: 1010_assertions_helper <name-of-assertion>" << endl;
        return 1;
    }

    std::string want(argv[1]);

    if(want == "assert_always") {
        trigger_assert_always();
    } else if (want == "assert_deprecated") {
        trigger_assert_deprecated();
    } else if (want == "assert_process_callback") {
        trigger_assert_process_callback();
    } else if (want == "assert_not_process_callback") {
        trigger_assert_not_process_callback();
    } else if (want == "warn_always") {
        trigger_warn_always();
    } else {
        assert(false);
    }

    return 0;
}
