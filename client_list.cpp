/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "client_list.hpp"
#include "Client.hpp"
#include "Mutex.hpp"
#include "MutexLocker.hpp"

#include <algorithm>
#include <cassert>
#include <cstring>

namespace JackSquat
{
    client_list_t::client_list_t()
    {
    }

    client_list_t::~client_list_t()
    {
        MutexLocker lock(m_mutex);
        iterator it;
        for( it=m_clients.begin() ; it != m_clients.end() ; ++it ) {
            delete *it;
        }
    }

    Client* client_list_t::make_new()
    {
        MutexLocker lock(m_mutex);
        T *t = new T;
        m_clients.insert(m_clients.end(), t);
        return t;
    }

    void client_list_t::remove(Client* client)
    {
        MutexLocker lock(m_mutex);
        iterator it;
        it = std::find(m_clients.begin(), m_clients.end(), client);
        if(it != m_clients.end()) {
            delete *it;
            m_clients.erase(it);
        } else {
            /* Tried to remove a non-existent client */
            assert(it != m_clients.end());
        }
    }

    bool client_list_t::has(const Client* c) const
    {
        MutexLocker lock(m_mutex);
        const_iterator it;
        if(!c)
            return false;
        it = std::find(m_clients.begin(), m_clients.end(), c);
        return (it != m_clients.end());
    }

    Client* client_list_t::find_by_name(const char* name) const
    {
        MutexLocker lock(m_mutex);
        const_iterator it;
        if(!name)
            return 0;
        for(it = m_clients.begin() ; it != m_clients.end() ; ++it) {
            if(0 == strcmp(name, (*it)->name())) {
                return *it;
            }
        }
        return 0;
    }

    bool client_list_t::lock() const
    {
        return m_mutex.lock();
    }

    bool client_list_t::unlock() const
    {
        return m_mutex.unlock();
    }

    Mutex& client_list_t::mutex() const
    {
        return m_mutex;
    }

} // namespace JackSquat
