/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "Client.hpp"
#include "jacksquat.h"
#include "jacksquat_assert.h"

#include <cassert>

namespace JackSquat
{
    Client::Client() :
        m_server(0),
        m_active(false)
    {
        m_process.callback = 0;
        m_process.arg = 0;
    }

    Client::~Client()
    {
    }

    const char* Client::name() const
    {
        assert( m_name.size() < JACKSQUAT_MAX_CLIENT_NAME );
        return m_name.c_str();
    }

    void Client::name(const char* n)
    {
        if(!n)
            return;
        m_name.assign(n, JACKSQUAT_MAX_CLIENT_NAME);
    }

    Server* Client::server()
    {
        return m_server;
    }

    void Client::server(Server* s)
    {
        m_server = s;
    }

    int Client::activate()
    {
        m_active = true;
        return 0;
    }

    int Client::deactivate()
    {
        m_active = false;
        return 0;
    }

    bool Client::active()
    {
        return m_active;
    }

    int Client::set_process_callback(JackProcessCallback cb, void* arg)
    {
        assert_always( ! active() );
        m_process.callback = cb;
        m_process.arg = arg;
        return 0;
    }

    const process_callback_t& Client::process()
    {
        return m_process;
    }

} // namespace JackSquat

