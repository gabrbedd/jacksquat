/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Global JACK SQUAT declarations and macros
 */
#ifndef JACKSQUAT_H
#define JACKSQUAT_H

#ifdef __cplusplus
extern "C" {
#endif

#if __GNUC__ >= 4
  #define JACKSQUAT_EXPORT __attribute__ ((visibility ("default")))
  #define JACKSQUAT_PRIVATE __attribute__ ((visibility ("hidden")))
#else
  #define JACKSQUAT_EXPORT
  #define JACKSQUAT_PRIVATE
#endif

#define JACKSQUAT_MAX_CLIENT_NAME 128

#ifdef __cplusplus
}
#endif

#endif // JACKSQUAT_H

