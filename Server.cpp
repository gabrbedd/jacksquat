/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "jacksquat.h"
#include "Server.hpp"
#include "Client.hpp"
#include "Thread.hpp"
#include "Config.hpp"
#include "MutexLocker.hpp"

#include "jacksquat_assert.h"

#include <memory>
#include <cassert>
#include <cstring>
#include <unistd.h>

using namespace std;

namespace JackSquat
{
    Server* server()
    {
        static auto_ptr<Server> instance;
        if(!instance.get()) {
            instance.reset( new Server );
        }
        return instance.get();
    }

    Server::Server()
    {
        Config* conf = config();
        m_run_thread.reset( new Thread );
        m_run_thread->set_run_main(Server::static_run_main, (void*)this);
        m_run_thread->start();
    }

    Server::~Server()
    {
        m_run_thread->cancel();
        m_run_thread->join();
    }

    Client* Server::client_open(const char *client_name,
                                      jack_options_t options,
                                      jack_status_t *status)
    {
        Client *c;
        c = m_clients.make_new();
        assert(c);
        assert_always(client_name);
        if(strlen(client_name) > JACKSQUAT_MAX_CLIENT_NAME) {
            warn_always("The client name exceeds the server max");
        }
        c->name(client_name);
        c->server(this);
        if(status) {
            *status = static_cast<jack_status_t>(0);
        }
        return c;
    }

    int Server::client_close(Client *client)
    {
        assert_always(client);
        assert_always( m_clients.has(client) );
        m_clients.remove(client);
        return 0;
    }

    int Server::activate(Client *client)
    {
        return client->activate();
    }

    int Server::deactivate(Client *client)
    {
        return client->deactivate();
    }

    void Server::static_run_main(void *arg)
    {
        Server *that = reinterpret_cast<Server*>(arg);
        that->run_main();
    }

    void Server::run_main()
    {
        const jack_nframes_t nframes = 64;
        client_list_t::iterator it;
        int stat;

        MutexLocker client_lock(m_clients.mutex());
        client_lock.unlock();

        while(true) {
            client_lock.relock();
            Thread::current_thread()->realtime(true);
            for( it = m_clients.begin() ; it != m_clients.end() ; ++it ) {
                Client *c = *it;
                if(c->active() && c->process().callback) {
                    stat = c->process().callback(nframes, c->process().arg);
                }
            }
            Thread::current_thread()->realtime(false);
            client_lock.unlock();

            usleep(20000);
        }
    }

} // namespace JackSquat
