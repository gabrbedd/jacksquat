/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "jacksquat_assert.h"
#include "Thread.hpp"
#include "Config.hpp"

#ifdef NDEBUG
#undefine NDEBUG
#endif

#include <cassert>

#include <iostream>
using namespace std;
using namespace JackSquat;

extern "C"
void assert_always(bool truth)
{
    assert(truth);
}

extern "C"
void assert_deprecated()
{
    if( config()->enforce_deprecated() )
        assert(false);
}

extern "C"
void assert_process_callback()
{
    if( config()->enforce_callback_api_rules() ) {
        Thread *t = Thread::current_thread();
        assert(t);
        assert(t->is_the_process_thread());
    }
}

extern "C"
void assert_not_process_callback()
{
    if( config()->enforce_callback_api_rules() ) {
        Thread *t = Thread::current_thread();
        if(t) {
            assert( ! t->is_the_process_thread() );
        }
    }
}

extern "C"
void warn_always(const char* msg)
{
    assert(msg);
    cout << msg << endl;
}
