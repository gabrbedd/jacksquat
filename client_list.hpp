/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef JACKSQUAT_CLIENT_LIST_HPP
#define JACKSQUAT_CLIENT_LIST_HPP

#include <deque>
#include "Mutex.hpp"

namespace JackSquat
{
    class Client;

    class client_list_t
    {
    public:
        client_list_t();
        ~client_list_t();

        Client* make_new();
        void remove(Client* client);

        // Query metods
        bool has(const Client* c) const;
        Client* find_by_name(const char* name) const;
        
    public:
        typedef Client T;
        typedef std::deque<T*> list_type;
        typedef list_type::iterator iterator;
        typedef list_type::const_iterator const_iterator;

        /* Must lock the list before using iterators */
        bool lock() const;
        bool unlock() const;
        Mutex& mutex() const;  /* for use with MutexLocker */

        iterator begin() { return m_clients.begin(); }
        const_iterator begin() const { return m_clients.begin(); }
        iterator end() { return m_clients.end(); }
        const_iterator end() const { return m_clients.end(); }

    private:
        list_type m_clients;
        mutable Mutex m_mutex;
    };

} // namespace JackSquat

#endif // JACKSQUAT_CLIENT_LIST_HPP
