/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef JACKSQUAT_MUTEXLOCKER_HPP
#define JACKSQUAT_MUTEXLOCKER_HPP

#include "Mutex.hpp"
#include <cassert>

namespace JackSquat
{
    class MutexLocker
    {
        bool m_locked;
        Mutex& m_mutex;

    public:
        MutexLocker(Mutex& m) throw(mutex_error) : 
            m_locked(false),
            m_mutex(m) {
            m_mutex.lock();
            m_locked = true;
        }

        ~MutexLocker() throw() {
            try {
                if(m_locked)
                    m_mutex.unlock();
            } catch(...) {
                /* suppress exception */
            }
        }

        /* Returns true on success */
        bool relock() throw(mutex_error) {
            bool rv = false;
            if(m_locked)
                throw mutex_error("Trying to relock() a locked mutex with MutexLocker");
            rv = m_mutex.lock();
            if(m_locked)
                throw mutex_error("Trying to relock() a locked mutex with MutexLocker");
            m_locked = true;
            return rv;
        }

        /* Returns true on success */
        bool try_lock() throw(mutex_error) {
            bool rv = false;
            if( m_mutex.try_lock() ) {
                m_locked = true;
                rv = true;
            }
            return rv;
        }

        bool unlock() throw(mutex_error) {
            /* will throw exception if not locked */
            m_locked = false;
            return m_mutex.unlock();
        }

    };

} // namespace JackSquat

#endif // JACKSQUAT_MUTEXLOCKER_HPP
